import { CurrencyRate } from './../../models/currencyrate';
import { BitcoinProvider } from './../../providers/bitcoin/bitcoin';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BitcoinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bitcoin',
  templateUrl: 'bitcoin.html',
})
export class BitcoinPage implements OnInit {
  Currency:string='AUD';
  Rates:CurrencyRate[]=[];
  ConversionRate:number;
  constructor(public navCtrl: NavController, public navParams: NavParams,private RatesService:BitcoinProvider) {
    
  }
  

  ngOnInit()
  {
    this.RefreshRates();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad BitcoinPage');
  }

  RefreshRates()
  {
    console.log('refresh')

    this.RatesService.GetRates().subscribe(
      data=>{
        //this.Rates=data.rates;
       
        this.ConversionRate=data.rates[this.Currency];
    },error=>{
          console.log(error)
    });
  }

}
