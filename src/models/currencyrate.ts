export interface CurrencyRate
{
    CurrencyName:string;
    Rate:number;
}