import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the BitcoinProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BitcoinProvider {

  private Root_Url:string='http://data.fixer.io/api/latest?access_key=fce765ba0431ff80e9108e02b2ce3e27';
  constructor(public http: HttpClient) {
    console.log('Hello BitcoinProvider Provider');
  }

  GetRates()
  {
    return this.http.get(this.Root_Url)
  }


}
